﻿using System;
using System.Threading;

namespace Input
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(Funzione1));
            t1.Start();

            Thread t2 = new Thread(new ParameterizedThreadStart(Funzione2));
            t2.Start("ciao");

            Thread t3 = new Thread(
                () =>
                {
                    Console.WriteLine("Terzo thread");
                });
            t3.Start();

            string s = "hello";
            int i = 4;
            Thread t4 = new Thread(
                () => Funzione4(s, i));
            t4.Start();

            Thread.Sleep(5000);
            Console.ReadKey();
        }

        static void Funzione1()
        {
            Console.WriteLine("Primo thread");
        }

        static void Funzione2(object o)
        {
            Console.WriteLine("Secondo thread, object o = " + o);
        }

        static void Funzione4(string p1, int p2)
        {
            Console.WriteLine("Quarto thread, p1 = " + p1 + ", p2 = " + p2);
        }

    }
}
