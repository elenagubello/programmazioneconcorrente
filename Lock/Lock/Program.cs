﻿using System;
using System.Threading;

namespace Lock
{
    class Program
    {
        static object locker = new object();

        static void Main(string[] args)
        {
            Thread tx = new Thread(WriteX);
            Thread ty = new Thread(WriteY);

            tx.Start();
            ty.Start();

            // Il thread principale non fa niente 
            Thread.Sleep(5000);
            Console.ReadKey();
        }

        static void WriteX()
        {
            lock (locker)
            {
                for (int i = 0; i < 20; i++)
                {
                    Console.WriteLine("x");
                }
            }
        }

        static void WriteY()
        {
            lock (locker)
            {
                for (int i = 0; i < 20; i++)
                {
                    Console.WriteLine(" y");
                }
            }
        }
    }
}
