with Ada.Text_Io;
with Ada.Strings.Bounded;
with Ada.Numerics.Float_Random;

procedure HelloTask is
   package Inner_Message is new
   Ada.Strings.Bounded.Generic_Bounded_Length(72);
   use Inner_Message;

   task type Messenger is
      entry Start(Message : in String);
   end Messenger;

  task body Messenger is
     use Ada.Numerics.Float_Random;
     Seed : Generator;
     Msg : Bounded_String;
  begin
     accept Start(Message : in String)
     do
        Msg := To_Bounded_String(Message);
     end Start;
     Reset(Seed);
     delay Duration(Random(Seed));
     Ada.Text_Io.Put_Line(To_String(Msg));
  end Messenger;

  M1 : Messenger;
  M2 : Messenger;
  M3 : Messenger;

begin

  M1.Start("Hello");
  M2.Start("From");
  M3.Start("Ada !");

end HelloTask;
