﻿using System;
using System.Threading;

namespace MultiThread
{
    class Program
    {
        public static void CallToChildThread()
        {
            Console.WriteLine("Child thread");

            int sleepfor = 5000;
            Console.WriteLine("Child thread in pausa per {0} secondi", sleepfor / 1000);
            Thread.Sleep(sleepfor);
            Console.WriteLine("Child thread riprende");
        }
        static void Main(string[] args)
        {
            ThreadStart childref = new ThreadStart(CallToChildThread);
            Console.WriteLine("Sono nel main. Sto creando un thread child");
            Thread childThread = new Thread(childref);
            childThread.Start();
            Console.ReadKey();
        }
    }
}
