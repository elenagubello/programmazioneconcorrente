﻿using System;
using System.Threading;

namespace Background
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(
                () =>
                {
                    for (int i = 0; i < 10; i++)
                        Console.WriteLine(i);
                    Console.ReadKey();
                });
            t.IsBackground = false;
            // t.IsBackground = true;
            t.Start();
        }
    }
}
