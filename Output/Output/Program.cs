﻿using System;
using System.Threading;

namespace Output
{
    class Program
    {
        static void Main(string[] args)
        {
            object value = "ciao";
            // usiamo un'espressione lambda
            Thread t = new Thread(
                () =>
                {
                    value = "hello";
                });
            t.Start();
            t.Join();
            Console.WriteLine(value);
            Console.ReadKey();
        }
    }
}
