﻿using System;
using System.Threading;

namespace Ciclo
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                int tmp = i;
                new Thread(
                    () => Console.WriteLine(tmp))
                    .Start();
            }
            Console.ReadKey();
            // come si può sistemare il codice per fare in modo che i numeri stampati 
            // dai vari thread siano in successione?
        }
    }
}
