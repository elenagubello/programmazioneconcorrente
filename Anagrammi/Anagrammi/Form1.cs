﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Anagrammi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static char[] p = new char[1];
        static string[] anag = new string[1];
        private void button1_Click(object sender, EventArgs e)
        {
            string parola = textBox1.Text;
            p = parola.ToCharArray();
            anag = new string[Fatt(p.Length)];
            Thread t = new Thread(Anagramma);
        }

        private static int Fatt(int n)
        {
            if (n < 2)
                return 1;
            return n * Fatt(n - 1);
        }

        private static void Anagramma(Object i)
        {
            // parametro di input = indice della lettera nella variabile parola
            // inizio gli anagrammi dalla lettera in posizione i
            int indice = (int)i;
            string primo = p[indice].ToString();
            for (int j = 0; j < p.Length && j != indice; j++)
            {
                primo += p[j].ToString();
            }
        }
    }
}
