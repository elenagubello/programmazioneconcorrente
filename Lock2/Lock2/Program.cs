﻿using System;
using System.Threading;

namespace Lock2
{
    class Program
    {
        static int contatore = 0;
        static object locker = new object();

        static void Main(string[] args)
        {
            //Crea e avvia il primo thread
            Thread thread1 = new Thread(new ThreadStart(MyThreadMethod1));
            thread1.Start();

            //Crea e avvia il secondo thread
            Thread thread2 = new Thread(new ThreadStart(MyThreadMethod2));
            thread2.Start();

            // Il thread principale non fa niente
            Thread.Sleep(5000);
            Console.ReadKey();
        }

        public static void MyThreadMethod1()
        {
            for (int i = 0; i < 20; i++)
            {
                lock (locker)
                {
                    contatore = 0;
                    Console.WriteLine("Thread 1: Variabile --> " + contatore);
                }
            }
        }

        public static void MyThreadMethod2()
        {
            for (int i = 0; i < 20; i++)
            {
                lock (locker)
                {
                    contatore++;
                    Console.WriteLine("Thread 2: Variabile --> " + contatore);
                }
            }
        }
    }
}
